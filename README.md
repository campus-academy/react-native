## Rendu React Native Swann LEROY

### Installer les dépendances

`yarn`

### Ensuite lancé l'application sur android 

`yarn android`



### Voici la liste des éléments présent dans l'application: 
- Une navigation ✅ tab bar, 2 stack (auth, et homeStack), et un écran seul Crypto screen
- Écran d’accueil ✅
- Communication avec une API extérieure ✅ (api coin gecko, récupération de la valeur du xtz sur les derniers 30 jours)
- Écran de listing ✅ la page home liste 3 cartes dans une FlatList
- Écran de détail du listing ✅ sur la page d'accueil au clique sur un élément de la liste
- Une implémentation de la Context API (ou Redux) ✅ Contexte api pour "l'authentification"
- Implémenter une fonction native (Caméra, Geoloc, Accéléromètre, tactile, etc...)✅ (caméra dans la vue détail d'une carte, le boutton en bas de page)
- Gestion d’erreur✅ (exemple try catch dans le service)
- Un peu de style✅ (j'ai fais au mieux ^^)


Élément non réaliser: 
- Challenge : Tracer un parcours utilisateur sur une carte avec la géoloc
  ❌
