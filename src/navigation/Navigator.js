import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AntDesign from 'react-native-vector-icons/AntDesign';

import HomeScreen from '../screens/HomeScreen';
import Colors from '../style/Colors';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from '../screens/LoginScreen';
import CryptoScreen from '../screens/CryptoScreen';
import {useAuth} from '../contexts/AuthContext';
import CardScreen from '../screens/CardScreen';

const TabNavigator = createBottomTabNavigator();

const AuthStackNavigator = createNativeStackNavigator();
const CardStackNavigator = createNativeStackNavigator();

const AuthNavigator = () => {
  return (
    <AuthStackNavigator.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <AuthStackNavigator.Screen name="Login" component={LoginScreen} />
    </AuthStackNavigator.Navigator>
  );
};

const CardStackScreen = () => {
  return (
    <CardStackNavigator.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <CardStackNavigator.Screen name="Home" component={HomeScreen} />
      <CardStackNavigator.Screen name="Card" component={CardScreen} />
    </CardStackNavigator.Navigator>
  );
};

const MainNavigator = () => {
  return (
    <TabNavigator.Navigator
      screenOptions={({route}) => ({
        tabBarStyle: {
          backgroundColor: Colors.dark,
          borderTopColor: 'transparent',
        },
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          const iconSize = focused ? 30 : 25;

          switch (route.name) {
            case 'HomeStack':
              iconName = 'creditcard';
              break;
            case 'Crypto':
              iconName = 'areachart';
              break;
            default:
              break;
          }

          return (
            <AntDesign
              name={iconName}
              size={iconSize}
              color={focused ? Colors.primary : Colors.light}
            />
          );
        },
        tabBarActiveTintColor: Colors.primary,
        tabBarInactiveTintColor: Colors.text,
        tabBarShowLabel: false,
        headerShown: false,
        tabBarHideOnKeyboard: Colors.dark,
      })}>
      <TabNavigator.Screen name="HomeStack" component={CardStackScreen} />
      <TabNavigator.Screen name="Crypto" component={CryptoScreen} />
      {/*<TabNavigator.Screen*/}
      {/*  name="Restaurants"*/}
      {/*  component={RestaurantNavigator}*/}
      {/*  options={{*/}
      {/*    headerShown: false,*/}
      {/*  }}*/}
      {/*/>*/}
    </TabNavigator.Navigator>
  );
};

const RootNavigator = () => {
  const {
    state: {isLoggedIn},
  } = useAuth();

  return isLoggedIn ? <MainNavigator /> : <AuthNavigator />;
};

export default RootNavigator;
