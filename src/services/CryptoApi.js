import axios from 'axios';

const api = axios.create({
  baseURL: 'https://api.coingecko.com/api/v3',
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const getCryptoHistory = async cryptoName => {
  const params = {
    vsCurrency: 'usd',
    days: 30,
    interval: 'daily',
  };

  try {
    const response = await api.get(
      `/coins/${cryptoName}/market_chart?vs_currency=${params.vsCurrency}&days=${params.days}&interval=${params.interval}`,
    );

    return response.data;
  } catch (error) {
    console.error(error);
  }
};
