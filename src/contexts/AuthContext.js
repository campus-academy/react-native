import React, {createContext, useContext, useReducer} from 'react';

// Initialisation du contexte
const AuthContext = createContext();

const actionTypes = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

// Etat initial du contexte
const initialState = {isLoggedIn: false, data: null};

/**
 * Traitement des actions
 * @param {*} state : Etat précédent l'action
 * @param {*} action : action pour mettre à jour l'état global
 * @returns nouvel état mit à jour
 */
const authReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.LOGIN:
      return {
        isLoggedIn: true,
      };
    case actionTypes.LOGOUT:
      return {
        isLoggedIn: false,
      };
    default:
      throw new Error(`Unhandled action type ${action.type}`);
  }
};

/**
 * Composant Provider : Distribue l'accès à l'état global aux enfants
 * @param {*} param0 : children : les enfants ayant besoin d'accéder à l'état
 * @returns Composant
 */
const AuthProvider = ({children}) => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  return (
    <AuthContext.Provider value={{state, dispatch}}>
      {children}
    </AuthContext.Provider>
  );
};

/**
 * Hook personnalisée permettant de valider l'appel du contexte
 * @returns { state, dispatch }
 */
const useAuth = () => {
  const context = useContext(AuthContext);

  // context = { state, dispatch }
  if (!context) {
    throw new Error('useCounter must be used inside a CounterProvider');
  }
  return context;
};

export {useAuth, AuthProvider, actionTypes};
