/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from "react";
import type {Node} from 'react';
import {StyleSheet} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import MainNavigator from './navigation/Navigator';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {AuthProvider} from './contexts/AuthContext';

const App: () => Node = () => {
  return (
    <AuthProvider>
      <NavigationContainer>
        <SafeAreaProvider>
          <MainNavigator />
        </SafeAreaProvider>
        {/*<SafeAreaView style={backgroundStyle}>*/}
        {/*  <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />*/}
        {/*  <ScrollView*/}
        {/*    contentInsetAdjustmentBehavior="automatic"*/}
        {/*    style={backgroundStyle}>*/}
        {/*    <Header />*/}
        {/*    <View*/}
        {/*      style={{*/}
        {/*        backgroundColor: isDarkMode ? Colors.black : Colors.white,*/}
        {/*      }}>*/}
        {/*      <Section title="Step One">*/}
        {/*        Edit <Text style={styles.highlight}>App.js</Text> to change this*/}
        {/*        screen and then come back to see your edits.*/}
        {/*      </Section>*/}
        {/*      <Section title="See Your Changes">*/}
        {/*        <ReloadInstructions />*/}
        {/*      </Section>*/}
        {/*      <Section title="Debug">*/}
        {/*        <DebugInstructions />*/}
        {/*      </Section>*/}
        {/*      <Section title="Learn More">*/}
        {/*        Read the docs to discover what to do next:*/}
        {/*      </Section>*/}
        {/*      <LearnMoreLinks />*/}
        {/*    </View>*/}
        {/*  </ScrollView>*/}
        {/*</SafeAreaView>*/}
      </NavigationContainer>
    </AuthProvider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
