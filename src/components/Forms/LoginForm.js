import React, {useState} from 'react';
import {Button, Text, View, StyleSheet} from 'react-native';
import TextInput from '../Inputs/TextInput';
import Colors from '../../style/Colors';

export default function LoginForm({onSubmit}) {
  const [credentials, setCredentials] = useState({
    identifier: 'test@test.fr',
    password: 'testtest',
  });

  return (
    <View>
      <Text style={styles.title}>Se connecter</Text>
      <TextInput
        iconName="person-circle"
        value={credentials.identifier}
        onChangeText={text => {
          setCredentials({...credentials, identifier: text});
        }}
      />
      <TextInput
        secureTextEntry
        iconName="lock-closed"
        value={credentials.password}
        onChangeText={text => {
          setCredentials({...credentials, password: text});
        }}
      />
      <Button
        color={Colors.primary}
        title="Se connecter"
        onPress={() => onSubmit(credentials)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 40,
    color: Colors.white,
    marginTop: 60,
    marginBottom: 60,
  },
});
