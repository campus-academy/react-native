import {StyleSheet} from 'react-native';
import Colors from '../../../style/Colors';

export default StyleSheet.create({
  container: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    borderWidth: 1,
    borderColor: Colors.primary,
    borderRadius: 5,
    marginVertical: 15,
  },
  input: {
    flex: 1,
    fontSize: 18,
    marginLeft: 5,
    color: Colors.white,
    borderColor: Colors.white,
  },
  icon: {
    paddingVertical: 8,
    paddingHorizontal: 15,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    borderRightWidth: 1,
    borderRightColor: Colors.primary,
    backgroundColor: Colors.primary,
    color: Colors.white,
  },
});
