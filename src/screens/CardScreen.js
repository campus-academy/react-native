import React from 'react';
import {Button, StyleSheet, Text, View, Image} from 'react-native';
import Colors from '../style/Colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {launchCamera} from 'react-native-image-picker';

export default function CardScreen({route, navigation}) {
  const [response, setResponse] = React.useState(null);
  const {item} = route.params;

  return (
    <SafeAreaView style={styles.view}>
      <View style={styles.header}>
        <Ionicons
          color={Colors.white}
          name={'arrow-back'}
          size={30}
          onPress={() => navigation.goBack()}
        />
      </View>
      <Text style={styles.title}>Détail de la carte</Text>
      <LinearGradient
        start={{x: 0, y: 0.75}}
        end={{x: 1, y: 0.25}}
        style={styles.card}
        colors={item.gradient}>
        <Text style={styles.holder}>Numéro de carte: {item.cardNumber}</Text>
        <Text style={styles.holder}>code secret: {item.cvv}</Text>
        <Text style={styles.name}>Nom de la banque: {item.name}</Text>
        <Text style={styles.name}>
          Date d'expiration: {item.expirationDate}
        </Text>
        <Text style={styles.totalAmount}>Propriétaire: {item.cardHolder}</Text>
      </LinearGradient>
      <Button
        style={styles.button}
        color={Colors.primary}
        title={'Prendre une photo de ma carte'}
        onPress={() =>
          launchCamera(
            {
              saveToPhotos: true,
              mediaType: 'photo',
              includeBase64: false,
              includeExtra: true,
            },
            result => setResponse(result),
          )
        }
      />
      {response?.assets &&
        response.assets.map(({uri}) => (
          <View key={uri} style={styles.imageContainer}>
            <Image
              resizeMode="cover"
              resizeMethod="scale"
              style={styles.image}
              source={{uri: uri}}
            />
          </View>
        ))}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.dark,
    display: 'flex',
    paddingHorizontal: 20,
  },
  header: {
    display: 'flex',
    alignItems: 'flex-start',
    width: '100%',
    marginTop: 20,
  },
  title: {
    fontSize: 40,
    color: Colors.white,
    marginTop: 60,
    marginBottom: 60,
  },
  holder: {
    color: Colors.white,
  },
  name: {
    color: Colors.white,
  },
  totalAmount: {
    color: Colors.white,
  },
  card: {
    width: '100%',
    height: 300,
    borderRadius: 20,
    padding: 30,
    marginBottom: 10,
    backgroundColor: 'red',
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    marginTop: 30,
    paddingHorizontal: 20,
  },
  imageContainer: {
    minHeight: 200,
    minWidth: 200,
    backgroundColor: Colors.light,
    marginVertical: 24,
    alignItems: 'center',
  },
  image: {
    minHeight: 200,
    width: 200,
  },
});
