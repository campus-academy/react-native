import React from 'react';
import {Button, View, StyleSheet} from 'react-native';
import LoginForm from '../components/Forms/LoginForm';
import {actionTypes, useAuth} from '../contexts/AuthContext';
import Colors from '../style/Colors';

export default function LoginScreen() {
  const {dispatch} = useAuth();

  const handleSubmit = async credentials => {
    dispatch({
      type: actionTypes.LOGIN,
      data: credentials,
    });
  };

  return (
    <View style={styles.view}>
      <LoginForm onSubmit={handleSubmit} />
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.dark,
    display: 'flex',
    paddingHorizontal: 20,
  },
});
