import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from '../style/Colors';
import {getCryptoHistory} from '../services/CryptoApi';
import {
  Chart,
  HorizontalAxis,
  Line,
  VerticalAxis,
} from 'react-native-responsive-linechart';

export default function CryptoScreen() {
  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [finalData, setFinalData] = useState([]);

  useEffect(() => {
    // declare the data fetching function
    const fetchData = async () => {
      const cryptoData = await getCryptoHistory('tezos');
      setData(cryptoData);
    };

    // call the function
    fetchData().catch(console.error);
  }, []);

  useEffect(() => {
    if (data) {
      for (let i = 0; i < data.prices.length; i++) {
        finalData.push({x: i, y: data.prices[i][1]});
      }
      setIsLoading(false);
    }
  }, [data, finalData]);

  return (
    <View style={styles.view}>
      <Text style={styles.title}>Crypto Screen</Text>
      {!isLoading && finalData.length > 0 ? (
        <>
          <Text style={styles.chartTitle}>
            Cours du Tezos sur les 30 derniers jours
          </Text>
          <Chart
            style={{height: 400, width: '100%', color: Colors.white}}
            xDomain={{min: 0, max: 30}}
            yDomain={{min: 1.3, max: 4}}
            padding={{left: 20, top: 10, bottom: 20, right: 10}}
            data={finalData}>
            <VerticalAxis
              tickValues={[
                1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5,
                2.6, 2.7, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8,
                3.9, 4,
              ]}
              theme={{
                grid: {
                  visible: true,
                  stroke: {
                    color: '#ccc',
                    width: 1,
                    opacity: 0.3,
                    dashArray: [],
                  },
                },
                ticks: {
                  visible: false,
                  stroke: {color: Colors.white, width: 2},
                  includeOriginTick: false,
                },
                labels: {
                  visible: true,
                  label: {
                    color: Colors.white,
                    fontSize: 10,
                    fontWeight: 200,
                    textAnchor: 'middle',
                    opacity: 1,
                    dx: -12,
                    dy: 0,
                    rotation: 0,
                  },
                },
              }}
            />
            <HorizontalAxis
              tickValues={Array.from(Array(31).keys())}
              theme={{
                grid: {
                  visible: true,
                  stroke: {
                    color: '#ccc',
                    width: 1,
                    opacity: 0.3,
                    dashArray: [],
                  },
                },
                ticks: {
                  visible: false,
                  stroke: {color: Colors.white, width: 2},
                  includeOriginTick: false,
                },
                labels: {
                  visible: true,
                  label: {
                    color: Colors.white,
                    fontSize: 10,
                    fontWeight: 200,
                    textAnchor: 'middle',
                    opacity: 1,
                    dx: 0,
                    dy: -15,
                    rotation: 0,
                  },
                },
              }}
            />
            <Line
              smoothing="yes"
              theme={{stroke: {color: Colors.primary, width: 1}}}
            />
          </Chart>
        </>
      ) : (
        <View>
          <Text style={styles.loading}>Chargement en cours</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.dark,
    display: 'flex',
    paddingHorizontal: 20,
  },
  chartTitle: {
    color: Colors.white,
  },
  title: {
    fontSize: 40,
    color: Colors.white,
    marginTop: 60,
    marginBottom: 60,
  },
  loading: {
    color: Colors.white,
  },
});
