import React from 'react';
import {FlatList, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Colors from '../style/Colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import LinearGradient from 'react-native-linear-gradient';

const DATA = [
  {
    id: 1,
    name: 'NEO DIGITAL',
    totalAmount: '10,586.00',
    cardHolder: 'Swann Leroy',
    gradient: ['#373b44', '#4286f4'],
    cardNumber: '4978 1782 8292 4444',
    cvv: 123,
    expirationDate: '12-12-2023',
  },
  {
    id: 2,
    name: 'N26',
    totalAmount: '36,200.00',
    cardHolder: 'Marius Sergent',
    gradient: ['#f953c6', '#b91d73'],
    cardNumber: '2222 3630 0918 5267',
    cvv: 642,
    expirationDate: '30-06-2022',
  },
  {
    id: 3,
    name: 'BANXO',
    totalAmount: '-1,200.00',
    cardHolder: 'Quentin Sabin',
    gradient: ['#11998e', '#38ef7d'],
    cardNumber: '4978 5555 9999 1345',
    cvv: 335,
    expirationDate: '01-07-2022',
  },
];

export default function HomeScreen({navigation}) {
  const Item = ({item}) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('Card', {
          item: item,
        })
      }>
      <LinearGradient
        start={{x: 0, y: 0.75}}
        end={{x: 1, y: 0.25}}
        style={styles.item}
        colors={item.gradient}>
        <Text style={styles.name}>{item.name}</Text>
        <View style={styles.amountContainer}>
          <Text style={styles.totalAmount}>{item.totalAmount}</Text>
          <Text style={styles.symbol}>€</Text>
        </View>
        <View>
          <Text style={styles.cardHolderLabel}>Card holder</Text>
          <Text style={styles.cardHolder}>{item.cardHolder}</Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );

  const renderItem = ({item}) => <Item item={item} />;

  return (
    <SafeAreaView style={styles.view}>
      <Text style={styles.title}>Wallet</Text>
      <FlatList
        style={styles.flatList}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.dark,
    display: 'flex',
    paddingHorizontal: 20,
  },
  flatList: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: 40,
    color: Colors.white,
    marginTop: 60,
    marginBottom: 60,
  },
  item: {
    height: 300,
    borderRadius: 40,
    padding: 30,
    marginBottom: 10,
    backgroundColor: 'red',
    display: 'flex',
    justifyContent: 'space-between',
  },
  itemTitle: {
    color: Colors.white,
  },
  name: {
    color: Colors.white,
    fontSize: 20,
  },
  amountContainer: {
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'row',
  },
  symbol: {
    fontSize: 25,
    color: Colors.white,
    marginLeft: 10,
  },
  totalAmount: {
    alignSelf: 'center',
    color: Colors.white,
    fontSize: 40,
    fontWeight: 'bold',
  },
  cardHolderLabel: {
    color: Colors.light,
  },
  cardHolder: {
    color: Colors.white,
    fontSize: 20,
  },
});
