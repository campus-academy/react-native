const colors = {
  primary: 'orange',
  secondary: 'teal',
  text: 'black',
  lightText: 'gray',
  light: 'lightgray',
  dark: 'black',
  white: 'white'
};

export default colors;
